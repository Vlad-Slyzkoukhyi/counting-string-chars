﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace CountingStringChars.Tests
{
    public sealed class GetNumberOfIdenticalDigitTests
    {
        // Test for an empty string - should throw an exception
        [Test]
        public void GetMaximumCountIdenticalDigit_StrIsEmpty_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => CountIdenticalObject.GetMaximumCountIdenticalDigit(string.Empty));
        }

        // Test for null input - should throw an exception
        [Test]
        public void GetMaximumCountIdenticalDigit_NullString_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => CountIdenticalObject.GetMaximumCountIdenticalDigit(null));
        }

        // Test with no digits
        [TestCase("A", ExpectedResult = 0)]
        [TestCase("abcdef", ExpectedResult = 0)]
        [TestCase("!@#$%^&*()", ExpectedResult = 0)]
        public int GetMaximumCountIdenticalDigit_NoDigits_ReturnsZero(string input)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalDigit(input);
        }

        // Test with single digit sequences
        [TestCase("1", ExpectedResult = 1)]
        [TestCase("12", ExpectedResult = 1)]
        [TestCase("123", ExpectedResult = 1)]
        [TestCase("123123123", ExpectedResult = 1)]
        public int GetMaximumCountIdenticalDigit_SingleDigits_ReturnsOne(string input)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalDigit(input);
        }

        // Test with multiple identical digits
        [TestCase("111", ExpectedResult = 3)]
        [TestCase("00", ExpectedResult = 2)]
        [TestCase("1112222", ExpectedResult = 4)]
        [TestCase("2222111", ExpectedResult = 4)]
        public int GetMaximumCountIdenticalDigit_MultipleIdenticalDigits_ReturnsMaxCount(string input)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalDigit(input);
        }

        // Test with mixed digits and characters
        [TestCase("a111b2222c333", ExpectedResult = 4)]
        [TestCase("1a1b1c", ExpectedResult = 1)]
        [TestCase("aaaa1bbbb1cccc1ddd", ExpectedResult = 1)]
        public int GetMaximumCountIdenticalDigit_MixedDigitsAndCharacters_ReturnsMaxCount(string input)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalDigit(input);
        }

        // Test with digits at the start, middle, and end
        [TestCase("aaa999bbb", ExpectedResult = 3)]
        [TestCase("zzz8888", ExpectedResult = 4)]
        [TestCase("7777zzz", ExpectedResult = 4)]
        public int GetMaximumCountIdenticalDigit_DigitsAtStartMiddleEnd_ReturnsMaxCount(string input)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalDigit(input);
        }

        // Test with alternating identical digits
        [TestCase("1212121212", ExpectedResult = 1)]
        [TestCase("2121121221", ExpectedResult = 2)]
        [TestCase("3344334433", ExpectedResult = 2)]
        public int GetMaximumCountIdenticalDigit_AlternatingIdenticalDigits_ReturnsOneOrTwo(string input)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalDigit(input);
        }

        // Test with a mix of digits and non-digits
        [TestCase("123abc123", ExpectedResult = 1)]
        [TestCase("1.11ab4,44cd5-56+6ef7=78", ExpectedResult = 2)]
        [TestCase("111#222b333&444d555", ExpectedResult = 3)]
        public int GetMaximumCountIdenticalDigit_MixedDigitsAndNonDigits_ReturnsMaxCount(string input)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalDigit(input);
        }

        // Test with empty spaces and multiply digits
        [TestCase("123456 000", ExpectedResult = 3)]
        [TestCase("abcd 1111111", ExpectedResult = 7)]
        [TestCase("%29 $%& 555", ExpectedResult = 3)]
        [TestCase("123 4567", ExpectedResult = 1)]
        public int GetMaximumCountIdenticalDigit_ParametersAreValid_ReturnsCharsCount(string inputString)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalDigit(inputString);
        }

        // Test with empty spaces and multiply digits separate by spaces
        [TestCase("123456 0 00", ExpectedResult = 2)]
        [TestCase("abcd 1111 111", ExpectedResult = 4)]
        [TestCase("%29 $%& 5 55", ExpectedResult = 2)]
        [TestCase("123 4567", ExpectedResult = 1)]
        public int GetMaximumCountIdenticalDigit_DigitsSeparateBySpaces_ReturnsCharsCount(string inputString)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalDigit(inputString);
        }
    }
}
