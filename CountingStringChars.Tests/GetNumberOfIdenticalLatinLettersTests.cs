﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace CountingStringChars.Tests
{
    public sealed class GetNumberOfIdenticalLatinLettersTests
    {
        // Test for an empty string - should throw an exception
        [Test]
        public void GetMaximumCountIdenticalLatinLetter_StrIsrEmpty_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => CountIdenticalObject.GetMaximumCountIdenticalLatinLetter(string.Empty));
        }

        // Test for an null input - should throw an exception
        [Test]
        public void GetMaximumCountIdenticalLatinLetter_StrIsNull_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => CountIdenticalObject.GetMaximumCountIdenticalLatinLetter(null));
        }

        // Test with no Latin letters
        [TestCase("0", ExpectedResult = 0)]
        [TestCase("123", ExpectedResult = 0)]
        [TestCase("!@#$%^&*()", ExpectedResult = 0)]
        public int GetMaximumCountIdenticalLatinLetter_NoLatinLetter_ReturnsZero(string inputString)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalLatinLetter(inputString);
        }

        // Test with single Latin letters sequences
        [TestCase("abcdefg", ExpectedResult = 1)]
        [TestCase("123sadf", ExpectedResult = 1)]
        [TestCase("123a", ExpectedResult = 1)]
        [TestCase("a123123123", ExpectedResult = 1)]
        public int GetMaximumCountLatinLetter_SingleLatinLetter_ReturnsOne(string inputString)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalLatinLetter(inputString);
        }

        // Test with multiple identical Latin letters
        [TestCase("aaabbbbcccddddddde", ExpectedResult = 7)]
        [TestCase("vvhhhwwwVXkKkkkkkk", ExpectedResult = 8)]
        [TestCase("yyyuuuuuuuuuiioks", ExpectedResult = 9)]
        [TestCase("aaaassgg", ExpectedResult = 4)]
        public int GetMaximumCountIdenticalLatinLetter_ParametersAreValid_ReturnsMaxCharsCount(string inputString)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalLatinLetter(inputString);
        }

        // Test with mixed Latin letters and characters
        [TestCase("a111b2222c333", ExpectedResult = 1)]
        [TestCase("1a1b1c", ExpectedResult = 1)]
        [TestCase("aaaa1bbbb1cccc1ddd", ExpectedResult = 4)]
        public int GetMaximumCountIdenticalLatinLetter_MixedDigitsAndLetters_ReturnsMaxCharsCount(string inputString)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalLatinLetter(inputString);
        }

        // Test with Latin letters at the start, middle, and end
        [TestCase("111aaa222", ExpectedResult = 3)]
        [TestCase("zzz8888", ExpectedResult = 3)]
        [TestCase("7777zzz", ExpectedResult = 3)]
        public int GetMaximumCountIdenticalLatinLetter_WithLatinLetterStartMiddleEnd_ReturnsCharsMaxCount(string inputString)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalLatinLetter(inputString);
        }

        // Test with alternating identical Latin letters
        [TestCase("abababababab", ExpectedResult = 1)]
        [TestCase("ababababaaba", ExpectedResult = 2)]
        [TestCase("abbabaaba", ExpectedResult = 2)]
        public int GetMaximumCountIdenticalLatinLetter_WithLatinAlternatingIdenticalLetter_ReturnsCharsMaxCount(string inputString)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalLatinLetter(inputString);
        }

        // Test with a mix of Latin letters and non-letter
        [TestCase("123abc123", ExpectedResult = 1)]
        [TestCase("1.11ab4,44ccd5-56+6ef7=78", ExpectedResult = 2)]
        [TestCase("111#222bbb333&444d555", ExpectedResult = 3)]
        public int GetMaximumCountIdenticalLatinLetter_MixLatinLetterAndNonLetter_ReturnsCharsMaxCount(string inputString)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalLatinLetter(inputString);
        }

        // Test with empty spaces and multiply Latin letters
        [TestCase("123%^ bbb", ExpectedResult = 3)]
        [TestCase("wwww% 65", ExpectedResult = 4)]
        [TestCase("abcd 1111111", ExpectedResult = 1)]
        public int GetMaximumCountIdenticalLatinLetter_WithSpacesAndMultiplyLetter_ReturnsCharsMaxCount(string inputString)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalLatinLetter(inputString);
        }

        // Test with empty spaces and multiply Latin letters separate by spaces
        [TestCase("123456 0 00 aa", ExpectedResult = 2)]
        [TestCase("abcd 1111 111", ExpectedResult = 1)]
        [TestCase("%29 $%& 5 55 sss", ExpectedResult = 3)]
        [TestCase("123 4567 asdffff", ExpectedResult = 4)]
        public int GetMaximumCountIdenticalLatinLetter_WithSpacesAndMultiplyLetterSeparateBySpaces_ReturnsCharsMaxCount(string inputString)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalLatinLetter(inputString);
        }

        // Test with camel case string
        [TestCase("aAabcde", ExpectedResult = 3)]
        [TestCase("aAabBbBbcde", ExpectedResult = 5)]
        [TestCase("aAabcdeEEEE", ExpectedResult = 5)]
        [TestCase("aaaaAabcde", ExpectedResult = 6)]
        public int GetMaximumCountIdenticalLatinLetter_WithCamelCaseString_ReturnsCharsMaxCount(string inputString)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalLatinLetter(inputString);
        }

        // Test with different language letter in string return just Latin Letter
        [TestCase("aaЩДЙши", ExpectedResult = 2)]
        [TestCase("ΑϔΤσssss", ExpectedResult = 4)]
        public int GetMaximumCountIdenticalLatinLetter_WithDifferent_ReturnsCharsMaxCount(string inputString)
        {
            return CountIdenticalObject.GetMaximumCountIdenticalLatinLetter(inputString);
        }
    }
}
