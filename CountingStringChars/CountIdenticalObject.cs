﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountingStringChars
{
    public static class CountIdenticalObject
    {
        public static int GetMaximumCountIdenticalDigit(string? inputString)
        {
            if (string.IsNullOrEmpty(inputString))
            {
                throw new ArgumentException("String is null or empty", nameof(inputString));
            }

            int maxCount = 0;
            int count;
            if (char.IsDigit(inputString[0]))
            {
                count = 1;
            }
            else
            {
                count = 0;
            }

            for (int i = 1; i < inputString.Length; i++)
            {
                if (char.IsDigit(inputString[i]))
                {
                    if (inputString[i] == inputString[i - 1])
                    {
                        count++;
                    }
                    else
                    {
                        maxCount = Math.Max(maxCount, count);
                        count = 1;
                    }
                }
            }

            maxCount = Math.Max(maxCount, count);
            return maxCount;
        }

        public static int GetMaximumCountIdenticalLatinLetter(string? inputString)
        {
            if (string.IsNullOrEmpty(inputString))
            {
                throw new ArgumentException("String is null or empty", nameof(inputString));
            }

            int maxCount = 0;
            int count;
            if ((inputString[0] >= 'A' && inputString[0] <= 'Z') || (inputString[0] >= 'a' && inputString[0] <= 'z'))
            {
                count = 1;
            }
            else
            {
                count = 0;
            }

            for (int i = 1; i < inputString.Length; i++)
            {
                if ((inputString[i] >= 'A' && inputString[i] <= 'Z') || (inputString[i] >= 'a' && inputString[i] <= 'z'))
                {
                    if (char.ToUpperInvariant(inputString[i]) == char.ToUpperInvariant(inputString[i - 1]))
                    {
                        count++;
                    }
                    else
                    {
                        maxCount = Math.Max(maxCount, count);
                        count = 1;
                    }
                }
            }

            maxCount = Math.Max(maxCount, count);
            return maxCount;
        }
    }
}
